import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Circle } from "rc-progress";
import Axios from "axios";

class App extends Component {
  state = { load: false };
  hourRate = 60 * 60 * 1000;
  hoursTarget = 2;
  target = this.hoursTarget * this.hourRate;
  fps = 10;

  componentDidMount() {
    this.uploadNewLog("visit");

    Axios.get("https://live-stopwatch.firebaseio.com/data.json").then(
      response => {
        let data = null;

        if (response.data === null) {
          data = { points: 0, last: 0, start: -1 };
        } else {
          data = { ...response.data };
        }
        this.setState({
          load: true,
          finished: false,
          last: 0,
          start: -1,
          points: 0,
          ...response.data,
          precentage: this.getPresentage(data.last, data.start)
        });
      }
    );

    setInterval(this.updateView, this.fps);
  }

  updateView = () => {
    this.setState(prevState => {
      let newState = { ...prevState };
      newState.precentage = this.getPresentage(newState.last, newState.start);
      return newState;
    });
  };

  getColor = precentage => {
    let first = [45, 183, 245];
    let second = [151, 201, 99];
    let color = first.map((_, index) => {
      return Math.floor(
        (first[index] * (100 - precentage) + second[index] * precentage) / 100
      );
    });
    return "rgb(" + color[0] + "," + color[1] + "," + color[2] + ")";
  };

  getPresentage = (last, start) => {
    let amount = last;
    if (start !== -1) {
      amount += Date.now() - start;
    }

    if (amount >= this.target) {
      if (!this.state.finished) this.finish();
      return 100;
    }

    return (amount / this.target) * 100;
  };

  play = () => {
    this.uploadNewLog("play");

    let newState = { ...this.state };
    newState.start = Date.now();
    newState.precentage = this.getPresentage(newState.last, newState.start);
    Axios.put("https://live-stopwatch.firebaseio.com/data.json", {
      points: newState.points,
      start: newState.start,
      last: newState.last
    });
    this.setState(newState);
  };

  pause = () => {
    this.uploadNewLog("pause");

    let newState = { ...this.state };
    newState.last += Date.now() - newState.start;
    newState.start = -1;
    newState.precentage = this.getPresentage(newState.last, newState.start);

    Axios.put("https://live-stopwatch.firebaseio.com/data.json", {
      points: newState.points,
      start: newState.start,
      last: newState.last
    });

    this.setState(newState);
  };

  reset = () => {
    this.uploadNewLog("reset");

    let newState = { ...this.state };
    newState.last = 0;
    newState.start = -1;
    newState.precentage = this.getPresentage(newState.last, newState.start);
    newState.finished = false;

    Axios.put("https://live-stopwatch.firebaseio.com/data.json", {
      points: newState.points,
      start: newState.start,
      last: newState.last
    });

    this.setState(newState);
  };

  finish = () => {
    let newState = { ...this.state };
    newState.last = 0;
    newState.start = -1;
    newState.precentage = this.getPresentage(newState.last, newState.start);
    newState.points += 1;

    Axios.put("https://live-stopwatch.firebaseio.com/data.json", {
      points: newState.points,
      start: newState.start,
      last: newState.last
    });

    this.uploadNewLog("finish");

    this.setState(prevState => {
      let newState = { ...this.state };
      newState.points += 1;
      newState.finished = true;
      return newState;
    });
  };

  uploadNewLog = (state) => {
    Axios.get("https://live-stopwatch.firebaseio.com/logs.json").then(
      response => {
        console.log(response);
        let newLogs = {};

        if (response.data !== null) newLogs = { ...response.data };
        newLogs[Object.keys(newLogs).length] = this.stateStampMessage(state);

        Axios.put("https://live-stopwatch.firebaseio.com/logs.json", newLogs);
      }
    );
  };

  stateStampMessage = (state) => {
    var currentdate = new Date();
    var datetime =
      "Local Finish Time: " +
      currentdate.getDate() +
      "/" +
      (currentdate.getMonth() + 1) +
      "/" +
      currentdate.getFullYear() +
      " @ " +
      currentdate.getHours() +
      ":" +
      currentdate.getMinutes() +
      ":" +
      currentdate.getSeconds();
    return { epoch: currentdate, msg: datetime, state: state };
  };

  render() {
    return (
      <div className="App" dir="rtl">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Live StopWatch</h1>
        </header>
        {this.state.load ? (
          <div>
            <h2 className="title">התקדמות יומית</h2>
            {this.state.finished ? (
              <div className="finishBox">
                סיימת להיום, כל הכבוד!! בשביל להתחיל מחדש לחץ על reset
              </div>
            ) : null}
            <div style={{ width: "25vw", margin: "auto" }}>
              {" "}
              <Circle
                percent={this.state.precentage}
                strokeWidth="3"
                strokeColor={this.getColor(this.state.precentage)}
              />
            </div>
            {this.state.finished ? null : (
              <div
                className="btn"
                onClick={() => {
                  if (this.state.start === -1) {
                    this.play();
                  } else {
                    this.pause();
                  }
                }}
              >
                {this.state.start === -1 ? "PLAY" : "PAUSE"}
              </div>
            )}
            <div className="btn resetBtn" onClick={this.reset}>
              RESET
            </div>
            <div className="target">היעד: {this.hoursTarget} שעות ביום</div>
            <div className="scoreboard">ימים שהושלמו: {this.state.points}</div>
          </div>
        ) : (
          <p>המערכת בטעינה...</p>
        )}
      </div>
    );
  }
}

export default App;
